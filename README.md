Introduction
------------

This directory contains the implementation of the CONVGEMM algorithm, which
computes a convolution using a gemm with an implicit im2col, for single
precision and Intel Haswell/Broadwell architectures.

It provides:

- The `libconvGemm.so` dynamic library, than can be used to execute the CONVGEMM
algorithm.

- The `testIm2Col` application, that compares the output of a naive convolution
  algorithm against the im2col+gemm approach and against the CONVGEMM algorithm.

- The `convEval` application, which evaluates the performance of the different
  implementations of the convolution operator for different CNNs.


Requirements
------------

Although the CONVGEMM algorithm does not require the BLIS library
(https://github.com/flame/blis), this library should be available in order to
compile and execute the testing applications.


Compilation
-----------

To compile the `libconvGemm.so` dynamic library, and the `testIm2Col` and
`convEval` applications, execute the following command:

    make

If the `libconvGemm.so` library is going to be installed outside of
`/usr/local/lib`, the `PREFIX` parameter can be used to specify a prefix
different of `/usr/local`. If this parameter is specified, the `PREFIX/lib`
directory will be added to the libraries search path and the `PREFIX/inc`
directory will be added to the included files search path. For example, the next
command will compile everything and will search for libraries and included files
also on `${HOME}/local/lib` and `${HOME}/local/inc`:

    make PREFIX="${HOME}/local"

The `EXTRACFLAGS` can also be used to specify additional directories where
libraries and included files should be searched. For example:

    make EXTRACFLAGS="-L/opt/lib -I/usr/local/include/blis"

Finally, the next options can be added to `EXTRACFLAGS` to tweak the behaviour
of `convEval`:

- `-DNONAIVE`: Do not execute the naive convolution algorithm (to save time).

- `-DLAYER_EVAL`: Measure the performance layer per layer and then aggregate the
                  results (instead of as a whole).



Installation
------------

Once compiled, execute the following command to install into `/usr/local` the
library and the test and evaluation applications:

    make install

By default, the `libconvGemm.so` library is installed into `/usr/local/lib`, and
the applications into `/usr/local/bin`. To change the default `/usr/local`
prefix, the `PREFIX` parameter can be used. For example, to install the library
and the applications in `${HOME}/local/lib` and `${HOME}/local/bin`,
respectively, the next command should be used instead:

    make PREFIX="${HOME}/local" install

Furthermore, if you require to use a different schema, it is also possible to
use the `LIBDIR` and `BINDIR` parameters to independently override their default
values, `PREFIX/lib` and `PREFIX/bin`, respectively.

Usage
-----

The CONVGEMM algorithm expects both input arrays as column major order
multidimensional arrays with the following data layout:

- Filter matrix: The filters should be arranged as a matrix with *k_n* rows and
  *k_h x k_w x c* columns stored in column major order. Each row contains a
  multidimensional array stored in column major order that represents a
  multichannel filter with *c* channels, *k_h* kernel height and *k_w* kernel
  width. The data layout of each filter is *k_h x k_w x c*.

- Input tensor: The input tensor is expected to be a multidimensional array
  stored in column major order with *h x w x c x b* data layout. If we
  understand the input tensor as a stack of multichannel images,  *h* is the
  image height, *w* the image width, *c* the number of channels of the image,
  and *b* the number of images in a batch.

The data layouts specified above follow the convention for column major
multidimensional arrays in which the left-most dimension is the closest in
memory.


Referencing
-----------

P. San Juan, A. Castelló, M. F. Dolz, P. Alonso-Jordá, and E. S. Quintana-Ortí,
"High Performance and Portable Convolution Operators for Multicore Processors,"
2020 IEEE 32nd International Symposium on Computer Architecture and High
Performance Computing (SBAC-PAD), Porto, Portugal, 2020, pp. 91-98, doi:
10.1109/SBAC-PAD49847.2020.00023.


License
-------

Copyright 2020 Universitat Politècnica de València

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
