/**
 * Implementations of the convolution related functions used by several testers
 * in the project.
 *
 * Copyright 2020 Universitat Politècnica de València
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author P. San Juan
 * @date 2020-11
 *
*/

#include "convCommon.h"

/**
 * Performs a multi-dimensional convolution using a naive algorithm.
 *
 * Performs a multi-dimensional convolution applying the filter matrix F to the
 * input tensor In.
 *
 * @param[in] h input tensor height
 * @param[in] w input tensor width
 * @param[in] c number of channels of input tensor
 * @param[in] b batch Size
 * @param[in] In 1D-array containing a flattened version of the input tensor
 * @param[in] kh kernel height
 * @param[in] kw kernel width
 * @param[in] kn kernel number
 * @param[in] F 1-D array containing filter/kernel matrix
 * @param[out] Out 1-D array containing the result of the convolution
 * @param[in] stride Stride to apply the kernels to the input tensor
 */
void convolutionNaive(const int h, const int w, const int c, const int b,
                      const float *In, const int kh, const int kw, const int kn,
                      const float *F, float *Out, const int stride)
{

    int ic, ikh, ikw, ih, iw, ib, ik, ho, wo, pad = 0; // Padding not supported
    float ZERO = (float) 0.0;

    ho = (h - kh + 2 * pad) / stride + 1; //integer division, note implicit floor
    wo = (w - kw + 2 * pad) / stride + 1; //integer division, note implicit floor

    bli_ssetv(BLIS_NO_CONJUGATE, ho * wo * kn * b, &ZERO, Out, 1);
    for(ib = 0; ib < b; ib++)
        for(ic = 0; ic < c; ic++)
            for(iw = 0; iw < wo; iw++)
                for(ih = 0; ih < ho; ih++)
                    for(ikw = 0; ikw < kw; ikw++)
                        for(ikh = 0; ikh < kh; ikh++)
                            for(ik = 0; ik < kn; ik++)
                                Out[ik + (ib * wo * ho + iw * ho + ih) * kn] +=
                                    In[ib * c * h * w + ic * h * w +
                                       (iw * stride + ikw) * h + (stride * ih + ikh)]
                                    * F[ik + (ic * kh * kw + ikw * kh + ikh) * kn];
}

/**
 * Performs a im2col transformation to the input tensor.
 *
 * Applies the im2Col transform to the input tensor. The im2col transform is
 * used to perform a convolution using the GEMM kernel.
 *
 * @param[in] h input tensor height
 * @param[in] w input tensor width
 * @param[in] c number of channels of input tensor
 * @param[in] b batch Size
 * @param[in] In 1D-array containing a flattened version of the input tensor
 * @param[in] kh kernel height
 * @param[in] kw kernel width
 * @param[in] stride Stride to apply the kernels to the input tensor
 * @param[out] Out Matrix (column major stored) containing the expanded matrix
 */
void im2Col(const int h, const int w, const int c, const int b, const float *In,
            const int kh, const int kw, const int stride, float *Out)
{
    int ic, ikh, ikw, ih, iw, ib,
        row, col, ho, wo, pad = 0; // Padding not yet supported

    ho = (h - kh + 2 * pad) / stride + 1; //integer division, note implicit floor
    wo = (w - kw + 2 * pad) / stride + 1; //integer division, note implicit floor

    int cSize = h * w, // Channel memory leap in input tensor
        coSize = ho * wo, // Channel memory leap in output matrix
        kSize = kh * kw, // Kernel memory leap (single channel)
        bSize = c * h * w, // Batch memory leap
        ckSize = c * kSize, // Kernels memory leap (all channels)
        posib, posic, posiw, posih, posikw, rowic, rowikw, colib, coliw;

    for(ib = 0; ib < b; ib++)
    {
        colib = ib * coSize;
        posib = ib * bSize;
        #pragma omp parallel for private (iw, ih, ikw, ikh, row, col, rowic, posic, coliw, posiw, posih, rowikw, posikw)
        for(ic = 0; ic < c; ic++)
        {
            rowic = ic * kSize;
            posic = ic * cSize + posib;
            for(iw = 0; iw < wo; iw++)
            {
                coliw = colib + iw * ho;
                posiw = iw * stride * h + posic;
                for(ih = 0; ih < ho; ih++)
                {
                    // OPT col = ib * coSize + iw * ho + ih;
                    col = coliw + ih;
                    posih = stride * ih;
                    for(ikw = 0; ikw < kw; ikw++)
                    {
                        rowikw = rowic + ikw * kh;
                        posikw = posiw + ikw * h;
                        for(ikh = 0; ikh < kh; ikh++)
                        {
                            // OPT row = ic * kSize + ikw * kh + ikh;
                            row = rowikw + ikh;
                            // OPT Out[row + col * ckSize] = In[ib * bSize + ic * cSize + (iw * stride + ikw) * h + (stride * ih + ikh)];
                            // OPT Out[row + col * ckSize] = In[posib + posic + posikw + posih + ikh];
                            Out[row + col * ckSize] = In[posikw + posih + ikh];
                        }
                    }
                }
            }
        }
    }
}

