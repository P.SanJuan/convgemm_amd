/**
 * GEMM and CONVGEMM code
 *
 * Implementation of the CONVGEMM algorithm first presented in
 * https://doi.org/10.1109/SBAC-PAD49847.2020.00023 and its auxiliary functions.
 *
 * Copyright 2020 Universitat Politècnica de València
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author P. San Juan
 * @date 2020-11
 *
*/

#include "convGemm.h"


/**
 * Allocates the packing buffers Ac_pack and Bc_pack with
 * proper size and alignment.
 *
 * @param[out] Ac_pack Buffer to contain packed portions of A.
 * @param[out] Bc_pack Buffer to contain packed portions of B.
 * @return 0 on success, 1 on allocation error.
 */
int alloc_pack_buffs(float** Ac_pack, float** Bc_pack)
{
    *Ac_pack =  (float *) aligned_alloc(ALIGN, BLOCK_MC * BLOCK_KC * sizeof(float));
    *Bc_pack =  (float *) aligned_alloc(ALIGN, BLOCK_KC * BLOCK_NC * sizeof(float));
    
    if(Ac_pack == NULL || Bc_pack == NULL)
        return 1;
    return 0;
}

/**
 * Single precision xpby for matrices
 *
 * Performs the operation  Y = X + beta * Y. All matrices are expected to be
 * stored in column major order.
 *
 * @param[in] m Number of rows of matrices X and Y.
 * @param[in] n Number of columns of matrices X and Y
 * @param[in] X Matrix to add.
 * @param[in] ldx Leading dimension of matrix X.
 * @param[in] beta Scalar to multiply Y.
 * @param[in,out] Y Input and output matrix.
 * @param[in] ldy Leading dimension of matrix Y.
 */
void sxpbyM(unsigned int m, unsigned int n, const float *restrict X, unsigned int ldx,
            const float *restrict beta, float *restrict Y, unsigned int ldy)
{
    unsigned int i, j;
    for(j = 0; j < n; j++)
        for(i = 0; i < m; i++)
            *(Y + i + j * ldy) =
                *(X + i + j * ldx) + *beta * *(Y + i + j * ldy);
}

/**
 * Sets all the elements of a single precision matrix to 0.
 *
 * @param[in] m Number of rows of matrix M.
 * @param[in] n Number of columns of matrix M.
 * @param[in,out] M Matrix to set.
 * @param[in] ldm Leading dimension of matrix M.
 */
void sset0sM(unsigned int m, unsigned int n, float *restrict M,
             unsigned int ldm)
{
    unsigned int i, j;

    #pragma omp parallel for private(i)
    for(j = 0; j < n; j++)
        for(i = 0; i < m; i++)
            *(M + i + j * ldm) = 0;
}

/**
 * Packing of simple precision matrix A.
 *
 * Packs a block of matrix A into a buffer A_pack in the proper data arrangement
 * that the microkernel needs.
 *
 * @param[in] A Pointer pointing to the position of A to start packing.
 * @param[in] lda Leading dimension of matrix a.
 * @param[in] A_pack Buffer containing the portion of A packed.
 * @param[in] m Height of the block to pack.
 * @param[in] k Width of the block to pack.
 */
void sPack_A(const float *A, unsigned int lda, float * restrict A_pack, unsigned int m,
             unsigned int k)
{
    float *A_pack_local;
    unsigned int skipPos;

    #pragma omp  parallel for private(A_pack_local, skipPos)
    for(unsigned int ic = 0; ic < m; ic += BLOCK_MR) 
    {

        A_pack_local = &A_pack[ic * k];
        unsigned int m_alg = min(BLOCK_MR, m - ic);
        skipPos = BLOCK_MR - m_alg;
        for(unsigned int pc = 0; pc < k; pc++)
        {
            for(unsigned int ir = 0; ir < m_alg; ir++)
            {
                A_pack_local[0] = A[(ic + ir) + pc * lda];
                A_pack_local++;
            }
            A_pack_local += skipPos;
        }
    }
}

/**
 * Packing of B + im2Col transform
 *
 * Packs matrix B = im2Col(In) into the buffer B_pack in the proper data
 * arrangement that the microkernel needs. Matrix B does not exist in memory and
 * the data packed into B_pack is read from the corresponding positions of the
 * input tensor (In), resulting in an on-the-fly im2col transform.
 *
 * @param[in] i Row index in matrix B of the first position of the block to pack.
 * @param[in] j Column index in matrix B of the first position of the block to pack.
 * @param[in] In Input tensor. Multidimensional array in column major order
 *               in the form h * w * c * b.
 * @param[out] B_pack Buffer containing the portion of B packed.
 * @param[in] k Height of the block to pack.
 * @param[in] n Width of the block to pack.
 * @param[in] c Number of channels of input tensor.
 * @param[in] h Input tensor height.
 * @param[in] w Input tensor width.
 * @param[in] ho Matrix B height.
 * @param[in] wo Matrix B width.
 * @param[in] kh Kernel height.
 * @param[in] kw Kernel width.
 * @param[in] stride Stride to apply the kernels to the input tensor.
 */
void sPack_im2Col(unsigned int i, unsigned int j, const float * In,
                  float *restrict B_pack, unsigned int k, unsigned int n,
                  unsigned int c,  unsigned int h, unsigned int w,
                  unsigned int ho, unsigned int wo,
                  unsigned int kh, unsigned int kw, unsigned int stride)
{
    unsigned int ic, ikw,
        ikh, // Row related indexes (regarding the phantom matrix)
        j_local, ib, iw, ih, // Col related indexes (regarding the phantom matrix)
        pos, pos_ic, pos_ib, pos_ic_ikw; // Position on the original image
    unsigned int pos_ic_ini, ikw_ini, ikh_ini, pos_ib_ini, iw_ini,
        ih_ini; // Initial values of indexes

    unsigned int cSize = h * w, // Channel memory leap in input tensor
        coSize = ho * wo, // Channel memory leap in matrix B
        kSize = kh * kw, // Kernel memory leap (single channel)
        bSize = c * h * w; // Batch memory leap

    unsigned int jc, pc, jr; // loop control indexes
    float *restrict B_pack_local;
    unsigned int skipPos;

    ic = i / kSize;
    ikw_ini = (i % kSize) / kh;
    ikh_ini = (i % kSize) % kh;
    pos_ic_ini = ic * cSize;

    #pragma omp parallel for private(B_pack_local, skipPos, j_local, pc, jr, ib, ih_ini, iw_ini, pos_ib_ini, pos_ic, ikw, pos_ic_ikw, ikh, pos_ib, iw, ih, pos) firstprivate(j)
    for(jc = 0; jc < n; jc += BLOCK_NR)
    {

        B_pack_local = &B_pack[jc * k];
        unsigned int n_alg = min(BLOCK_NR, n - jc);
        skipPos = BLOCK_NR - n_alg;

        j_local = j + jc;
        ib = j_local / coSize;
        iw_ini = (j_local % (coSize)) / ho;
        ih_ini = (j_local % (coSize)) % ho;
        pos_ib_ini = ib * bSize;

        // ih_ini = ih_ini + jc

        pos_ic = pos_ic_ini;
        ikw = ikw_ini;
        pos_ic_ikw = ikw * h + pos_ic;
        for(pc = 0, ikh = ikh_ini; pc < k; pc++, ikh++)
        {
            if(ikh == kh)
            {
                ikh = 0;
                ikw++;
                pos_ic_ikw += h; // OPT pos_ic_ikw = ikw* h +pos_ic
                if(ikw == kw) 
                {
                    ikw = 0;
                    pos_ic += cSize; // OPT ic++; pos_ic = ic * cSize;
                    pos_ic_ikw = pos_ic; // OPT pos_ic_ikw = ikw *h + pos_ic;
                }
            }

            pos_ib = pos_ib_ini;
            iw = iw_ini;
            for(jr = 0, ih = ih_ini; jr < n_alg; jr++, ih++)
            {
                if(ih == ho)
                {
                    ih = 0;
                    iw++;
                    if(iw == wo)
                    {
                        iw = 0;
                        pos_ib += bSize; // OPT ib++; pos_in = ib*bSize;
                    }
                }
                // OPT pos = ib * bSize  + ic * cSize + (iw * stride + ikw) *h + (ih * stride + ikh);
                // OPT pos = pos_ib + pos_ic + (iw * stride * h + pos_ikw) + (ih * stride + ikh);
                pos = pos_ib + pos_ic_ikw + iw * stride * h + (ih * stride + ikh);

                B_pack_local[0] = In[pos];
                B_pack_local++;
            }
            B_pack_local += skipPos;
        }
        // ih_ini = ih;
        // iw_ini = iw;
        // pos_ib_ini = pos_ib;
    }
}

/**
 * Simple precision matrix matrix multiplication with implicit im2col.
 *
 * Performs a matrix matrix product in the form C = alpha * AB + beta * C, where
 * B = im2col(In). Expects matrices stored in column major order.
 *
 * @param[in] kh Kernel height.
 * @param[in] kw Kernel width.
 * @param[in] c Number of channels of input tensor.
 * @param[in] kn Kernel number.
 * @param[in] alpha Scalar alpha.
 * @param[in] A Matrix A, lda assumed as kn. Filter matrix stored in column
 *              major order with kn * hh * kw * c data layout.
 * @param[in] h Input tensor height.
 * @param[in] w Input tensor width.
 * @param[in] b Batch size.
 * @param[in] stride Stride to apply the kernels to the input tensor.
 * @param[in] In 1D-array containing a flattened version of the input tensor.
 *               Multidimensional array in column major order in the
 *               form h * w * c * b.
 * @param[in] beta Scalar beta.
 * @param[in,out] C Matrix C, ldc assumed as kn.
 * @param[in] Ac_pack Workspace for the packing of A (only for allocation purposes).
 * @param[in] Bc_pack Workspace for the packing of B (only for allocation purposes).
 */
void sconvGemm(unsigned int kh, unsigned int kw, unsigned int c,
               unsigned int kn,
               float alpha, const float *A,
               unsigned int h, unsigned int w, unsigned int b,
               unsigned int stride,
               const float *In, float beta,
               float *restrict C,
               float *restrict Ac_pack, float *restrict Bc_pack)
{

    const float *Ac;
    float *Cc;
    float *Ar, *Br;
    float *Cr;
    float betaInner, zero = 0.0;

    unsigned int ho, wo, pad = 0; // Padding currently unsupported

    ho = (h - kh + 2 * pad) / stride + 1; //integer division, note implicit floor
    wo = (w - kw + 2 * pad) / stride + 1; //integer division, note implicit floor

    unsigned int m = kn,
        n = ho * wo * b,
        k = kh * kw * c;

    unsigned int lda = kn,
        ldc = kn;

    float CBuff[BLOCK_MR * BLOCK_NR];
    sset0sM(BLOCK_MR, BLOCK_NR, CBuff, BLOCK_MR);

    for(unsigned int jc = 0; jc < n; jc += BLOCK_NC) 
    {

        unsigned int n_alg = min(BLOCK_NC, n - jc);
        for(unsigned int pc = 0; pc < k; pc += BLOCK_KC)
        {

            unsigned int k_alg = min(BLOCK_KC, k - pc);
            if(pc >= BLOCK_KC) //Check beta
                betaInner = 1.0;
            else
                betaInner = beta;

            sPack_im2Col(pc, jc, In, Bc_pack, k_alg, n_alg, c, h, w, ho, wo,
                         kh, kw, stride);  // PACK B

            for(unsigned int ic = 0; ic < m; ic += BLOCK_MC)
            {

                unsigned int m_alg = min(BLOCK_MC, m - ic);
                float *Ac_pack_local = Ac_pack;

                Ac = &A[ic + pc * lda];
                sPack_A(Ac, lda, (float *) Ac_pack_local, m_alg, k_alg); // PACK A

                Cc = &C[ic + jc * ldc];

                #pragma omp parallel for  private(Ar, Br, Cr, CBuff)
                for(unsigned jr = 0; jr < n_alg; jr += BLOCK_NR)
                {
                    unsigned int nr_alg = min(BLOCK_NR, n_alg - jr);
                    for(unsigned int ir = 0; ir < m_alg; ir += BLOCK_MR)
                    {
                        unsigned int mr_alg = min(BLOCK_MR, m_alg - ir);
                        Ar = &Ac_pack_local[ir * k_alg];
                        Br = &Bc_pack[jr * k_alg];
                        Cr = &Cc[ir + jr * ldc];

                        if(mr_alg == BLOCK_MR && nr_alg == BLOCK_NR)
                            sgemm_asm_6x16(k_alg, &alpha, Ar, Br, &betaInner, Cr, 1, ldc);
                        else  //Micro-kernel cannot be applied
                        {
                            sgemm_asm_6x16(k_alg, &alpha, Ar, Br, &zero, CBuff, 1, BLOCK_MR);
                            sxpbyM(mr_alg, nr_alg, CBuff, BLOCK_MR, &betaInner, Cr, ldc);
                        }
                    }
                }
            }
        }
    }
}
