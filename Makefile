#
# convGemm-AMD Makefile
#
# Copyright 2020 Universitat Politècnica de València
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# @author Sergio Barrachina Mir; P. San Juan
# @date 2020-11
#

# ------------------
# Makefile variables
# ------------------

# Installation and search directories
PREFIX = /usr/local
LIBDIR = $(abspath $(PREFIX)/lib)
INCDIR = $(abspath $(PREFIX)/include)
BINDIR = $(abspath $(PREFIX)/bin)

# Compiler and compilation options
CC = gcc
CFLAGS = -fpic -fopenmp $(OPTCFLAGS) -L. $(EXTRACFLAGS) -L$(LIBDIR) -I$(INCDIR)
OPTCFLAGS = -O3 -ftree-vectorize $(ARCHCFLAGS) \
			-funsafe-math-optimizations -ffp-contract=fast -fomit-frame-pointer
ARCHCFLAGS = -mavx2 -mfma -mfpmath=sse -march=core-avx2
LINKEDLIBS = -lblis -lm -lconvGemm

# The applications, library and micro kernels to be compiled
APPS := testIm2Col convEval
LIB := libconvGemm.so
uKOBJS = gemm_asm_d6x8.o


# --------------
# Makefile rules
# --------------

.PHONY: all install clean

all: $(APPS) $(LIB)

%.o: %.c %.h
	$(CC) -c -o $@ $< $(CFLAGS)

libconvGemm.so: convGemm.o $(uKOBJS)
	$(CC) -shared -fopenmp -o $@ $^

$(APPS) : % : %.o convCommon.o $(LIB)
	$(CC) -o $@ $< convCommon.o $(CFLAGS) $(LINKEDLIBS)

install: $(APPS) $(LIB)
	@mkdir -p $(BINDIR)
	@for app in $(APPS); do                           \
		echo "Installing $${app} into $(BINDIR)/" ;   \
		install -c -m 0755 $${app} $(BINDIR);         \
	 done
	@echo "Installing libconvGemm.so into $(LIBDIR)/"
	@mkdir -p $(LIBDIR)
	@install -c -m 0755 libconvGemm.so $(LIBDIR)
	@echo "Installing convGemm.so into $(INCDIR)/"
	@mkdir -p $(INCDIR)
	@install -c -m 0644 convGemm.h $(INCDIR)

clean:
	@echo "Cleaning"
	rm -f *.o
