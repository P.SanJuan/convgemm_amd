/**
 * GEMM and CONVGEMM declarations
 *
 * Declaration of the CONVGEMM algorithm first presented in
 * https://doi.org/10.1109/SBAC-PAD49847.2020.00023 and its auxiliary functions.
 *
 * Copyright 2020 Universitat Politècnica de València
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author P. San Juan
 * @date 2020-11
 *
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>
#include <omp.h>
#include <stdint.h>

// Simple precision BLIS block sizes for HASWELL
#define BLOCK_NC 4080
#define BLOCK_KC 256
#define BLOCK_MC 168
#define BLOCK_NR 16
#define BLOCK_MR 6
#define ALIGN 32


#ifndef maxmin
    #define max(a, b) (((a)>(b))?(a):(b))
    #define min(a, b) (((a)<(b))?(a):(b))
#endif


// To allocate the packing buffers Ac_pack and Bc_pack
int alloc_pack_buffs(float** Ac_pack, float** Bc_pack);


// Simple precision convolution gemm
void sconvGemm(unsigned int kh, unsigned int kw, unsigned int c,
               unsigned int kn,
               float alpha, const float *A,
               unsigned int h, unsigned int w, unsigned int b,
               unsigned int stride,
               const float *In, float beta,
               float *restrict C,
               float *restrict Ac_pack, float *restrict Bc_pack);


// Microkernels
void sgemm_asm_6x16(unsigned int k, float *restrict alpha, 
                    float *restrict a, float *restrict b,
                    float *restrict beta, float *restrict c,
                    unsigned int rs_c, unsigned int cs_c);


// Packing routine sPack_A
void sPack_A(const float *A, unsigned int lda, 
             float *restrict A_pack, unsigned int m, unsigned int k);


// Packing routine sPack_im2Col
void sPack_im2Col(unsigned int i, unsigned int j, const float *In,
                  float *restrict B_pack, unsigned int k, unsigned int n,
                  unsigned int c, unsigned int h, unsigned int w,
                  unsigned int ho, unsigned int wo,
                  unsigned int kh, unsigned int kw, unsigned int stride);
