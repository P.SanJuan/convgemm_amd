/**
 * Declarations of the convolution related functions used by several testers in
 * the project.
 *
 * Copyright 2020 Universitat Politècnica de València
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author P. San Juan
 * @date 2020-11
 *
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <blis.h>

#ifndef maxmin
    #define max(a, b) (((a)>(b))?(a):(b))
    #define min(a, b) (((a)<(b))?(a):(b))
#endif

void convolutionNaive( int h,  int w,  int c,  int b, const float *In,
                       int kh,  int kw,  int kn, const float *F,
                       float *Out,  int stride);

void im2Col( int h,  int w,  int c,  int b, const float *In,
             int kh,  int kw,  int stride, float *Out);

